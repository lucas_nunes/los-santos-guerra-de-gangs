#include <YSI\y_hooks>
Autenticar_Login( playerid )
{
	new tmpp[ 129 ];
	cache_get_field_content( 0, "FraseEntrada", tmpp ); format( Frase[ playerid ], 129, "%s", tmpp ) ;
	cache_get_field_content( 0, "pass", tmpp ); format( pInfo[ playerid ][ pass ], 129, "%s", tmpp ) ;
	cache_get_field_content_int( 0, "ID", pInfo[ playerid ][ Codigo ] ) ;
    cache_get_field_content( 0, "email", tmpp ); format( pInfo[ playerid ][ Email ], 129, "%s", tmpp ) ;
    // --
    PlayerTextDrawSetString( playerid, LoginPlayer[ 0 ], GetPlayerNameEx( playerid ));
	for( new I; I < sizeof LoginPlayer; I++) PlayerTextDrawShow( playerid, LoginPlayer[ I ] );
    for( new I = 0; I < sizeof LoginPadrao; I++) TextDrawShowForPlayer( playerid, LoginPadrao[ I ] ) ;
    // -0-
    new strin[ 128 ], playerson = Iter_Count( Player );
	if(strcmp("N/A", Frase[playerid], true))
	{
		format( strin, sizeof strin, "{08FF00}���{FFFFFF} %s Entrou no servidor (%s) {08FF00}[ %d/%d ].", GetPlayerNameEx( playerid ), Frase[ playerid ], playerson, GetMaxPlayers());
	}
	else
	{
		format( strin, sizeof strin, "{08FF00}���{FFFFFF} %s Entrou no servidor {08FF00}[ %d/%d ].", GetPlayerNameEx( playerid ), playerson, GetMaxPlayers());
	}
	SendClientMessageToAll( -1, strin ) ;
	return 1;
}
hook OnPlayerConnect( playerid )
{
	LoginPlayer[ 0 ] = CreatePlayerTextDraw(playerid,243.000000, 198.000000, "_");
	PlayerTextDrawBackgroundColor(playerid,LoginPlayer[ 0 ], 255);
	PlayerTextDrawFont(playerid,LoginPlayer[ 0 ], 1);
	PlayerTextDrawLetterSize(playerid,LoginPlayer[ 0 ], 0.200000, 1.000000);
	PlayerTextDrawColor(playerid,LoginPlayer[ 0 ], -1);
	PlayerTextDrawSetOutline(playerid,LoginPlayer[ 0 ], 0);
	PlayerTextDrawSetProportional(playerid,LoginPlayer[ 0 ], 1);
	PlayerTextDrawSetShadow(playerid,LoginPlayer[ 0 ], 1);
	PlayerTextDrawSetSelectable(playerid,LoginPlayer[ 0 ], 0);

	LoginPlayer[ 1 ] = CreatePlayerTextDraw(playerid,243.000000, 224.000000, "~w~Digite sua senha");
	PlayerTextDrawBackgroundColor(playerid,LoginPlayer[ 1 ], 255);
	PlayerTextDrawFont(playerid,LoginPlayer[ 1 ], 2);
	PlayerTextDrawLetterSize(playerid,LoginPlayer[ 1 ], 0.200000, 1.000000);
	PlayerTextDrawColor(playerid,LoginPlayer[ 1 ], -1);
	PlayerTextDrawSetOutline(playerid,LoginPlayer[ 1 ], 0);
	PlayerTextDrawSetProportional(playerid,LoginPlayer[ 1 ], 1);
	PlayerTextDrawSetShadow(playerid,LoginPlayer[ 1 ], 1);
	PlayerTextDrawTextSize( playerid, LoginPlayer[ 1 ], 385.000000, 10.000000 ) ;
	PlayerTextDrawSetSelectable(playerid,LoginPlayer[ 1 ], 1);	
	return 1;
}
hook OnGameModeInit()
{
	LoginPadrao[ 0 ] = TextDrawCreate(680.000000, -10.000000, "_");
	TextDrawBackgroundColor(LoginPadrao[ 0 ], 255);
	TextDrawFont(LoginPadrao[ 0 ], 1);
	TextDrawLetterSize(LoginPadrao[ 0 ], 0.500000, 55.000000);
	TextDrawColor(LoginPadrao[ 0 ], -1);
	TextDrawSetOutline(LoginPadrao[ 0 ], 0);
	TextDrawSetProportional(LoginPadrao[ 0 ], 1);
	TextDrawSetShadow(LoginPadrao[ 0 ], 1);
	TextDrawUseBox(LoginPadrao[ 0 ], 1);
	TextDrawBoxColor(LoginPadrao[ 0 ], 471604479);
	TextDrawTextSize(LoginPadrao[ 0 ], -40.000000, 0.000000);
	TextDrawSetSelectable(LoginPadrao[ 0 ], 0);

	LoginPadrao[ 1 ] = TextDrawCreate(440.000000, 140.000000, "_");
	TextDrawBackgroundColor(LoginPadrao[ 1 ], 255);
	TextDrawFont(LoginPadrao[ 1 ], 1);
	TextDrawLetterSize(LoginPadrao[ 1 ], 0.000000, 22.000000);
	TextDrawColor(LoginPadrao[ 1 ], -1);
	TextDrawSetOutline(LoginPadrao[ 1 ], 0);
	TextDrawSetProportional(LoginPadrao[ 1 ], 1);
	TextDrawSetShadow(LoginPadrao[ 1 ], 1);
	TextDrawUseBox(LoginPadrao[ 1 ], 1);
	TextDrawBoxColor(LoginPadrao[ 1 ], 866844544);
	TextDrawTextSize(LoginPadrao[ 1 ], 180.000000, 30.000000);
	TextDrawSetSelectable(LoginPadrao[ 1 ], 0);

	LoginPadrao[ 2 ] = TextDrawCreate(439.000000, 163.000000, "_");
	TextDrawBackgroundColor(LoginPadrao[ 2 ], 255);
	TextDrawFont(LoginPadrao[ 2 ], 1);
	TextDrawLetterSize(LoginPadrao[ 2 ], 0.560000, 19.399997);
	TextDrawColor(LoginPadrao[ 2 ], -1);
	TextDrawSetOutline(LoginPadrao[ 2 ], 0);
	TextDrawSetProportional(LoginPadrao[ 2 ], 1);
	TextDrawSetShadow(LoginPadrao[ 2 ], 1);
	TextDrawUseBox(LoginPadrao[ 2 ], 1);
	TextDrawBoxColor(LoginPadrao[ 2 ], 471604479);
	TextDrawTextSize(LoginPadrao[ 2 ], 181.000000, -70.000000);
	TextDrawSetSelectable(LoginPadrao[ 2 ], 0);

	LoginPadrao[ 3 ] = TextDrawCreate(378.000000, 197.000000, "_");
	TextDrawBackgroundColor(LoginPadrao[ 3 ], 255);
	TextDrawFont(LoginPadrao[ 3 ], 1);
	TextDrawLetterSize(LoginPadrao[ 3 ], 0.560000, 1.399997);
	TextDrawColor(LoginPadrao[ 3 ], -1);
	TextDrawSetOutline(LoginPadrao[ 3 ], 0);
	TextDrawSetProportional(LoginPadrao[ 3 ], 1);
	TextDrawSetShadow(LoginPadrao[ 3 ], 1);
	TextDrawUseBox(LoginPadrao[ 3 ], 1);
	TextDrawBoxColor(LoginPadrao[ 3 ], -208);
	TextDrawTextSize(LoginPadrao[ 3 ], 234.000000, -110.000000);
	TextDrawSetSelectable(LoginPadrao[ 3 ], 0);

	LoginPadrao[ 4 ] = TextDrawCreate(378.000000, 223.000000, "_");
	TextDrawBackgroundColor(LoginPadrao[ 4 ], 255);
	TextDrawFont(LoginPadrao[ 4 ], 1);
	TextDrawLetterSize(LoginPadrao[ 4 ], 0.560000, 1.399997);
	TextDrawColor(LoginPadrao[ 4 ], -1);
	TextDrawSetOutline(LoginPadrao[ 4 ], 0);
	TextDrawSetProportional(LoginPadrao[ 4 ], 1);
	TextDrawSetShadow(LoginPadrao[ 4 ], 1);
	TextDrawUseBox(LoginPadrao[ 4 ], 1);
	TextDrawBoxColor(LoginPadrao[ 4 ], -208);
	TextDrawTextSize(LoginPadrao[ 4 ], 234.000000, -110.000000);
	TextDrawSetSelectable(LoginPadrao[ 4 ], 0);

	LoginPadrao[ 5 ] = TextDrawCreate(249.000000, 179.000000, "Logue-se para iniciar sua sessao:");
	TextDrawBackgroundColor(LoginPadrao[ 5 ], 255);
	TextDrawFont(LoginPadrao[ 5 ], 1);
	TextDrawLetterSize(LoginPadrao[ 5 ], 0.200000, 1.000000);
	TextDrawColor(LoginPadrao[ 5 ], -1);
	TextDrawSetOutline(LoginPadrao[ 5 ], 0);
	TextDrawSetProportional(LoginPadrao[ 5 ], 1);
	TextDrawSetShadow(LoginPadrao[ 5 ], 1);
	TextDrawSetSelectable(LoginPadrao[ 5 ], 0);

	LoginPadrao[ 6 ] = TextDrawCreate(259.000000, 146.000000, "Los Santos Guerra de ~r~~h~GANGS");
	TextDrawBackgroundColor(LoginPadrao[ 6 ], -251);
	TextDrawFont(LoginPadrao[ 6 ], 1);
	TextDrawLetterSize(LoginPadrao[ 6 ], 0.200000, 1.000000);
	TextDrawColor(LoginPadrao[ 6 ], -1);
	TextDrawSetOutline(LoginPadrao[ 6 ], 1);
	TextDrawSetProportional(LoginPadrao[ 6 ], 1);
	TextDrawSetSelectable(LoginPadrao[ 6 ], 0);

	LoginPadrao[ 7 ] = TextDrawCreate(269.000000, 283.000000, "Esqueceu sua senha?");
	TextDrawBackgroundColor(LoginPadrao[ 7 ], 255);
	TextDrawFont(LoginPadrao[ 7 ], 1);
	TextDrawLetterSize(LoginPadrao[ 7 ], 0.200000, 1.000000);
	TextDrawColor(LoginPadrao[ 7 ], -1);
	TextDrawSetOutline(LoginPadrao[ 7 ], 0);
	TextDrawSetProportional(LoginPadrao[ 7 ], 1);
	TextDrawSetShadow(LoginPadrao[ 7 ], 1);
	TextDrawTextSize( LoginPadrao[ 7 ], 310.000000, 10.000000 ) ;
	TextDrawSetSelectable(LoginPadrao[ 7 ], 1);

	LoginPadrao[ 8 ] = TextDrawCreate(378.000000, 248.000000, "_");
	TextDrawBackgroundColor(LoginPadrao[ 8 ], 255);
	TextDrawFont(LoginPadrao[ 8 ], 1);
	TextDrawLetterSize(LoginPadrao[ 8 ], 0.790000, 2.799997);
	TextDrawColor(LoginPadrao[ 8 ], -1);
	TextDrawSetOutline(LoginPadrao[ 8 ], 0);
	TextDrawSetProportional(LoginPadrao[ 8 ], 1);
	TextDrawSetShadow(LoginPadrao[ 8 ], 1);
	TextDrawUseBox(LoginPadrao[ 8 ], 1);
	TextDrawBoxColor(LoginPadrao[ 8 ], 866844512);
	TextDrawTextSize(LoginPadrao[ 8 ], 234.000000, -110.000000);
	TextDrawSetSelectable(LoginPadrao[ 8 ], 0);

	LoginPadrao[ 9 ] = TextDrawCreate(378.000000, 248.000000, "_");
	TextDrawBackgroundColor(LoginPadrao[ 9 ], 255);
	TextDrawFont(LoginPadrao[ 9 ], 1);
	TextDrawLetterSize(LoginPadrao[ 9 ], 0.790000, 1.199997);
	TextDrawColor(LoginPadrao[ 9 ], -1);
	TextDrawSetOutline(LoginPadrao[ 9 ], 0);
	TextDrawSetProportional(LoginPadrao[ 9 ], 1);
	TextDrawSetShadow(LoginPadrao[ 9 ], 1);
	TextDrawUseBox(LoginPadrao[ 9 ], 1);
	TextDrawBoxColor(LoginPadrao[ 9 ], 866844512);
	TextDrawTextSize(LoginPadrao[ 9 ], 234.000000, -110.000000);
	TextDrawSetSelectable(LoginPadrao[ 9 ], 0);

	LoginPadrao[ 10 ] = TextDrawCreate(293.000000, 256.000000, "LOGIN");
	TextDrawBackgroundColor(LoginPadrao[ 10 ], 255);
	TextDrawFont(LoginPadrao[ 10 ], 1);
	TextDrawLetterSize(LoginPadrao[ 10 ], 0.200000, 1.000000);
	TextDrawColor(LoginPadrao[ 10 ], -1);
	TextDrawSetOutline(LoginPadrao[ 10 ], 0);
	TextDrawSetProportional(LoginPadrao[ 10 ], 1);
	TextDrawSetShadow(LoginPadrao[ 10 ], 1);
	TextDrawTextSize( LoginPadrao[ 10 ], 310.000000, 10.000000 ) ;
	TextDrawSetSelectable(LoginPadrao[ 10 ], 1);

	LoginPadrao[ 11 ] = TextDrawCreate(264.000000, 325.000000, "GTA BRASIL RPG 0.1 BETA");
	TextDrawBackgroundColor(LoginPadrao[ 11 ], 255);
	TextDrawFont(LoginPadrao[ 11 ], 1);
	TextDrawLetterSize(LoginPadrao[ 11 ], 0.200000, 1.000000);
	TextDrawColor(LoginPadrao[ 11 ], -1);
	TextDrawSetOutline(LoginPadrao[ 11 ], 0);
	TextDrawSetProportional(LoginPadrao[ 11 ], 1);
	TextDrawSetShadow(LoginPadrao[ 11 ], 1);
	TextDrawSetSelectable(LoginPadrao[ 11 ], 0);

	LoginPadrao[ 12 ] = TextDrawCreate(440.000000, 154.000000, "_");
	TextDrawBackgroundColor(LoginPadrao[ 12 ], 255);
	TextDrawFont(LoginPadrao[ 12 ], 1);
	TextDrawLetterSize(LoginPadrao[ 12 ], 0.790000, 0.699997);
	TextDrawColor(LoginPadrao[ 12 ], -1);
	TextDrawSetOutline(LoginPadrao[ 12 ], 0);
	TextDrawSetProportional(LoginPadrao[ 12 ], 1);
	TextDrawSetShadow(LoginPadrao[ 12 ], 1);
	TextDrawUseBox(LoginPadrao[ 12 ], 1);
	TextDrawBoxColor(LoginPadrao[ 12 ], 32);
	TextDrawTextSize(LoginPadrao[ 12 ], 180.000000, -110.000000);
	TextDrawSetSelectable(LoginPadrao[ 12 ], 0);
	return 1;
}
