#define ENVIAR_EMAIL  	76
#define D_LOGIN         77
#define	D_EMAIL   		78
#define	D_REGISTRO   	79
#define	D_CONFSENHA  	80

// --
enum PLAYER_DATA  //Player Infos
{
	Codigo,
	pass[ 128 ],
	Email[ 128 ],
	LoginsFalsos,
	Patentes,
 	Spree,
 	Mortes,
 	GangsDominadas,
 	Admin,
 	Procurado,
 	ModoLag,
	VIP,
	VSegundos,
	VMinutos,
	VHoras,
	VDias,
	VTime,
	Skin,
	Dinheiro,
	x1ganhos,
	x1perdidos,
	Shots,
	Visual,
	EventosGanhos,
	Preso,
	TempoPreso,
	Hud,
	Duel,
	motivoBanned[40],
	adminBanned[21],
	dataBanned[10],
	horaBanned[10],
	ipBanned[16]
};
new pInfo[MAX_PLAYERS][PLAYER_DATA];
new bool: PlayerDigitouLogin[ MAX_PLAYERS ];
// 
new bool: DigitouSenha[ MAX_PLAYERS ];
new bool: ConfSenha[ MAX_PLAYERS ];
new bool: DigitouEmail[ MAX_PLAYERS ];
// --
new bool: t_PainelOpen[ MAX_PLAYERS ];
new bool:Logado[MAX_PLAYERS] = false;
new bool:Registrado[MAX_PLAYERS] = false;
new bool:Novato[MAX_PLAYERS];
new JailTimer[MAX_PLAYERS];
new Frase[MAX_PLAYERS][50];
new bool:EmPainel[MAX_PLAYERS] = false;

/* ~~ TextDraws ~~ */
new PlayerText: RegisterPlayer[ 3 ];
new Text: RegisterGlobal[ 13 ];
new Text: LoginPadrao[ 13 ];
new PlayerText: LoginPlayer[ 2 ];
