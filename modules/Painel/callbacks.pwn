#include <YSI\y_hooks>

hook OnPlayerConnect(playerid)
{
	SetTimerEx("MostrarLoginAndRegistro", 2000 , 0 , "i", playerid);
	return 1;
} 
forward MostrarLoginAndRegistro(playerid);
public MostrarLoginAndRegistro(playerid)
{
    GameTextForPlayer(playerid, "___", 1000, 3);
	new cameras = random(6);
	switch(cameras)
	{
	    case 0:
	    {
	        InterpolateCameraPos(playerid, 1659.967773, -2033.977050, 114.032539, 1135.730957, -2037.127075, 72.670089, 16000);
			InterpolateCameraLookAt(playerid, 1655.063720, -2033.958374, 113.058158, 1130.767089, -2037.001953, 72.082855, 16000);
	    }
	    case 1:
	    {
     		InterpolateCameraPos(playerid, 2421.358886, -1913.913940, 146.060165, 1440.372314, -948.492980, 91.525047, 16000);
			InterpolateCameraLookAt(playerid, 2418.880126, -1909.575683, 145.872589, 1439.539184, -943.582214, 91.088485, 16000);
	    }
	    case 2:
	    {
	        InterpolateCameraPos(playerid, 341.452056, -885.602783, 125.134780, 2298.640380, -1265.082519, 62.921581, 16000);
			InterpolateCameraLookAt(playerid, 345.813354, -888.017761, 124.751502, 2297.206542, -1260.372070, 62.052291, 16000);
	    }
	    case 3:
	    {
			InterpolateCameraPos(playerid, 314.303131, -1381.531860, 76.257392, 1310.936767, -1281.293701, 32.765281, 16000);
			InterpolateCameraLookAt(playerid, 319.224151, -1380.911132, 75.626266, 1315.668701, -1281.267333, 31.150241, 16000);
	    }
	    case 4:
	    {
	        InterpolateCameraPos(playerid, -2039.425903, 138.526473, 66.198432, -1193.369873, 1044.919067, 100.876213, 16000);
			InterpolateCameraLookAt(playerid, -2036.099609, 142.258758, 66.276123, -1196.788330, 1041.272460, 100.748977, 16000);
	    }
	    case 5:
	    {
	        InterpolateCameraPos(playerid, -2723.561523, -289.622131, 156.303756, -2678.777587, 1387.122802, 159.650634, 16000);
			InterpolateCameraLookAt(playerid, -2723.376953, -284.627075, 156.427978, -2678.762207, 1392.122680, 159.623397, 16000);
	    }
	}
	new query[70];
    mysql_format( db, query, sizeof query, "SELECT * FROM users WHERE name = '%e'", GetPlayerNameEx( playerid ));
    mysql_tquery( db, query, "Autenticar", "d", playerid);
	return 1;
}
forward Autenticar( playerid );
public Autenticar( playerid )
{
	SelectTextDraw( playerid, 0xFF0000AA ) ;
	t_PainelOpen[ playerid ] = true;
	if( cache_num_rows() == 1)
	{
		Autenticar_Login( playerid );
	}
	else Autenticar_Register( playerid );
	return 1;
}
PasswordEncode( string[], size)
{
	for(new s; s <= size; s++)
	{
		if(s ^ size) string[s] = ']';
		else string[s] = EOS;
	}
	return 1;
}
hook OnPlayerClickTextDraw(playerid, Text:clickedid)
{
	if(_:clickedid == INVALID_TEXT_DRAW) 
	{
		if( t_PainelOpen[ playerid ] == true) return SelectTextDraw( playerid, 0xFF0000AA  ) ;
	}
	if( clickedid == RegisterGlobal[ 10 ])
	{
		if( DigitouSenha[ playerid ] == true && DigitouEmail[ playerid ] == true && ConfSenha[ playerid ] == true)
        {
	 		for( new I; I < sizeof RegisterPlayer; I++) PlayerTextDrawHide( playerid, RegisterPlayer[ I ] );
	        for( new I = 0; I < sizeof RegisterGlobal; I++) TextDrawHideForPlayer( playerid, RegisterGlobal[ I ] ) ;
			t_PainelOpen[ playerid ] = false;
			CancelSelectTextDraw( playerid );
			PlayerPlaySound(playerid,1057, 0.0, 0.0, 0.0);
            Registrado[playerid] = true;
            Frase[playerid] = "N/A";
			SetPlayerScore(playerid, 0 );
			GivePlayerGrana(playerid, 10000);
			// --
			new query[ 500 ];
        	mysql_format( db, query, sizeof query, "INSERT INTO `users` (`name`, `pass`, `email`) VALUES('%e', '%e', '%e');", GetPlayerNameEx( playerid ), pInfo[ playerid ][ pass ], pInfo[ playerid ][ Email ] );
        	mysql_tquery( db, query, "DadosRegistrados", "i", playerid );
			// --
			AparecerSelecionarGang( playerid );

	        DigitouSenha[ playerid ] = false, DigitouEmail[ playerid ] = false, ConfSenha[ playerid ] = false;
		}
		else SendClientMessage( playerid, 0xFF0000AA, "[ x ] - Porfavor preencha todos os campos a cima.");
	}
	if( clickedid == LoginPadrao[ 7 ] ) ShowPlayerDialog ( playerid, ENVIAR_EMAIL, DIALOG_STYLE_MSGBOX , "Recuperação de Senha", "{FFFFFF}Deseja enviar sua senha para seu email?\nPara enviar clique em 'Enviar'\nPara não enviar clique em 'Cancelar'.", "Enviar", "Cancelar" ) ;
	if( clickedid == LoginPadrao[ 10 ] ) //Entrar
	{
		if( PlayerDigitouLogin[ playerid ] == false) return SendClientMessage( playerid, 0xFF0000AA, "[ x ] - Digite sua senha para logar em sua conta.");
	   	for( new I; I < sizeof LoginPlayer; I++) PlayerTextDrawHide( playerid, LoginPlayer[ I ] );
    	for( new I = 0; I < sizeof LoginPadrao; I++) TextDrawHideForPlayer( playerid, LoginPadrao[ I ] ) ;
    	t_PainelOpen[ playerid ] = false;
	 	StopAudioStreamForPlayer( playerid );
	 	CancelSelectTextDraw( playerid );
		Novato[playerid] = false;
        CarregarFile( playerid );
        AparecerSelecionarGang( playerid );
	}
	return 1;
}
hook OnPlayerDisconnect( playerid, reason)
{
	SalvarDados(playerid);
	t_PainelOpen[ playerid ] = false;
	DigitouSenha[ playerid ] = false;
	ConfSenha[ playerid ] = false;
	DigitouEmail[ playerid ] = false;

	PlayerDigitouLogin[ playerid ] = false;
	return 1;
}
forward CarregarFile( playerid );
public CarregarFile( playerid )
{
    new query[ 100 ];
    mysql_format( db, query, sizeof query, "SELECT * FROM `users` WHERE `name` = '%e' LIMIT 1", GetPlayerNameEx( playerid ));
    mysql_tquery( db, query, "LoadFile", "d", playerid ) ;
    return 1;
}
forward LoadFile( playerid );
public LoadFile( playerid )
{
   	ResetPlayerGrana(playerid);
    GivePlayerGrana( playerid, cache_get_field_content_int( 0, "Grana"));
    SetPlayerScore( playerid, cache_get_field_content_int( 0, "Score"));
   	pInfo[ playerid ][ Admin ] = cache_get_field_content_int( 0, "Admin");
   	pInfo[ playerid ][ Spree ] = cache_get_field_content_int( 0, "Spree");
   	pInfo[ playerid ][ GangsDominadas ] = cache_get_field_content_int( 0, "gDominados");
   	pInfo[ playerid ][ Procurado ] = cache_get_field_content_int( 0, "Procurado");
   	pInfo[ playerid ][ ModoLag ] = cache_get_field_content_int( 0, "SemLag");
   	pInfo[ playerid ][ Mortes ] = cache_get_field_content_int( 0, "Mortes");
	// VIP
	pInfo[ playerid ][ VIP ] = cache_get_field_content_int( 0, "VIP");
   	pInfo[ playerid ][ VSegundos ] = cache_get_field_content_int( 0, "VSegundos");
   	pInfo[ playerid ][ VMinutos ] = cache_get_field_content_int( 0, "VMinutos");
   	pInfo[ playerid ][ VHoras ] = cache_get_field_content_int( 0, "VHoras");
   	pInfo[ playerid ][ VDias ] = cache_get_field_content_int( 0, "VDias");
	// --
    pInfo[ playerid ][ x1ganhos ] = cache_get_field_content_int( 0, "X1Wins");
    pInfo[ playerid ][ x1perdidos ] = cache_get_field_content_int( 0, "X1Loser");
    pInfo[ playerid ][ EventosGanhos ] = cache_get_field_content_int( 0, "EventosWins");
    pInfo[ playerid ][ Visual ] = cache_get_field_content_int( 0, "Visual");
    pInfo[ playerid ][ Hud ] = cache_get_field_content_int( 0, "HudColor");
    KillTimer( JailTimer[ playerid ] );
    pInfo[ playerid ][ Preso ] = cache_get_field_content_int( 0, "Preso");
    pInfo[ playerid ][ TempoPreso ] = cache_get_field_content_int( 0, "TempoPreso");
	// ==
	new ultlogin[ 64 ], dataregister[ 64 ], tmpp[ 129 ];
	cache_get_field_content( 0, "UltimoLogin", tmpp ); format( ultlogin, 129, "%s", tmpp ) ;
	cache_get_field_content( 0, "Register", tmpp ); format( dataregister, 129, "%s", tmpp ) ;
	// --
	format( ultlogin, 64, "Ultimo Login:{FFFFFF} %s", ultlogin ) ;
	SendClientMessage(playerid, 0x33AAFFAA, ultlogin );
	// --
	format( dataregister, 64, "Data do registro:{FFFFFF} %s", dataregister ) ;
	SendClientMessage(playerid, 0x33AAFFAA, dataregister );
	// --
	Logado[playerid] = true;
	Registrado[playerid] = true;
	SendClientMessage(playerid, 0xA9C4E4FF, "Logado com sucesso! Todos seus dados foram carregados. Tenha um ótimo jogo!");
	PlayerPlaySound(playerid,1057, 0.0, 0.0, 0.0);
	TogglePlayerControllable(playerid, 1);
	if(pInfo[playerid][VIP])
	{
	    EmPainel[playerid] = false;
		pInfo[playerid][VTime] = SetTimerEx("VipCont", 1000 , 1 , "i", playerid);
		SendClientMessage(playerid, 0x33AAFFAA, "[VIP]: Voc� logou como um jogador vip, acesse seu painel digitando /PainelVip.");
	}
	new date[ 84 ];
	new hora, minuto, segundo, ano, mes, dia ;
	getdate( ano, mes, dia );
	gettime( hora, minuto, segundo );
	format( date, 84, "%02d/%02d/%02d As %02d:%02d:%02d", dia, mes, ano, hora, minuto, segundo ) ;
	// ---
	new query[ 100 ];
    mysql_format( db, query, sizeof query, "UPDATE `users` SET `UltimoLogin`='%s' WHERE `name` = '%e'", date, GetPlayerNameEx( playerid ));
    mysql_tquery( db, query, "", "" ) ;
	return 1;
}
SalvarDados( playerid )
{
	if(Logado[playerid] == true || Registrado[playerid] == true)
	{
 		/* SALVAR ARQUIVOS */
	    new query[ 2400 ];
		strcat( query, "UPDATE `users` SET `Grana`='%d',`Score`='%d',`Spree`='%d',`gDominados`='%d',`Procurado`='%d',`SemLag`='%d', `Mortes`='%d', `VIP`='%d', `VSegundos`='%d', `VMinutos`='%d', `VHoras`='%d', `VDias`='%d',");
		strcat( query, "`Admin`='%d', `X1Wins`='%d', `X1Loser`='%d', `EventosWins`='%d', `Visual`='%d', `Preso`='%d', `TempoPreso`='%d', `HudColor`='%d', `FraseEntrada`='%s' WHERE `name`='%s'");
		format( query, sizeof query, query, GranaDoPlayer(playerid),
		GetPlayerScore(playerid),
		pInfo[playerid][Spree],
		pInfo[playerid][GangsDominadas],
	 	pInfo[playerid][Procurado],
	 	pInfo[playerid][ModoLag],
	 	pInfo[playerid][Mortes],
	 	pInfo[playerid][VIP],
	 	pInfo[playerid][VSegundos],
	 	pInfo[playerid][VMinutos],
	 	pInfo[playerid][VHoras],
	 	pInfo[playerid][VDias],
	 	pInfo[playerid][Admin],
	 	pInfo[playerid][x1ganhos],
	 	pInfo[playerid][x1perdidos],
	 	pInfo[playerid][EventosGanhos],
	 	pInfo[playerid][Visual],
	 	pInfo[playerid][Preso],
	 	pInfo[playerid][TempoPreso],
	 	pInfo[playerid][Hud],
	 	Frase[playerid],
		GetPlayerNameEx( playerid )); /* -- 59 */
   		mysql_tquery( db, query, "", "" );
	}
    return 1;
}
/* -- Callbacks */
hook OnDialogResponse( playerid, dialogid, response, listitem, inputtext[ ])
{
   	if(strfind(inputtext, "%") ^ -1) return SendClientMessage(playerid, 0xFF0000AA, "[ x ] - Go crash another server douche!"),Kick( playerid );

	switch( dialogid )
	{
		case D_LOGIN:
		{
			if(response )
			{
				if( strcmp( pInfo[ playerid ][ pass ], inputtext, false) == 0)
				{
					if(!strlen( inputtext )) ShowPlayerDialog( playerid, D_LOGIN, DIALOG_STYLE_PASSWORD , "Senha Incorreta" , "Senha Incorreta, Digite novamente a senha para tentar logar em sua conta.", "Login" , "X" ) ;
					else
					{
						PlayerDigitouLogin[ playerid ] = true;
						PasswordEncode( inputtext, strlen( inputtext ));
		        		PlayerTextDrawSetString( playerid, LoginPlayer[ 1 ], inputtext );
					}
				}
				else
				{
					pInfo[ playerid ][ LoginsFalsos ]++;
					if(pInfo[ playerid ][ LoginsFalsos ] >= 3) return ShowPlayerDialog ( playerid, ENVIAR_EMAIL, DIALOG_STYLE_MSGBOX , "Recuperação de Senha", "{FFFFFF}Deseja enviar sua senha para seu email?\nPara enviar clique em 'Enviar'\nPara não enviar clique em 'Cancelar'.", "Enviar", "Cancelar" ) ;
					else ShowPlayerDialog( playerid, D_LOGIN, DIALOG_STYLE_PASSWORD , "Senha Incorreta" , "Senha Incorreta, Digite novamente a senha para tentar logar em sua conta.", "Login" , "X" ) ;
				}
			}
			else
			{
				Kick( playerid );
			}
		}
	    case D_CONFSENHA:
	    {
			if( response )
			{
				if( DigitouSenha[ playerid ] == false) return SendClientMessage( playerid, 0xFF0000AA, "[ x ] - Você não digitou a senha corretamente.");
				if( strcmp( inputtext, pInfo[ playerid ][ pass ] , true) == 0)
				{
					PasswordEncode( inputtext, strlen( inputtext ));
					PlayerTextDrawSetString( playerid, RegisterPlayer[ 1 ], inputtext );
					ConfSenha[ playerid ] = true;
				}
				else {
					ShowPlayerDialog( playerid, D_CONFSENHA, DIALOG_STYLE_PASSWORD, "{FF0000}#{FFFFFF} Senha Incorreta!", "Tente novamente\nConfirme a senha corretamente.!", "Confirmar", "x" );
				}
	    	}
	    }
   		case D_EMAIL:
	 	{
			if( response )
		    {
				if( !strlen( inputtext ))
	      		{
			        new Stringl[ 250 ] ;
	        		format( Stringl, sizeof Stringl, "{FFFFFF}Registrando seu email\n\tRegistre seu email para caso você esqueça sua {FF0000}'SENHA'{FFFFFF}\nVocê poderar recupera-la." ) ;
	        		ShowPlayerDialog( playerid, D_EMAIL, DIALOG_STYLE_INPUT , "{FFFFFF}Cadastro Email", Stringl, "Cadastrar", "Cancelar" ) ;
					return 0x01;
				}
	      		if( strfind( inputtext, "@", true, 1 ) == -1 )
	      		{
		    	    new Stringl[ 250 ] ;
		        	format( Stringl , sizeof Stringl, "{FFFFFF}Registrando seu email\n\tRegistre seu email para caso você esqueça sua {FF0000}'SENHA'{FFFFFF}\nVocê poderar recupera-la." ) ;
		        	ShowPlayerDialog( playerid, D_EMAIL, DIALOG_STYLE_INPUT , "{FFFFFF}Cadastro Email" , Stringl , "Cadastrar" , "Cancelar" ) ;
	    	    	SendClientMessage( playerid, 0xFF0000AA, "[ x ] - O uso do Email deve conter obrigatoriamente, hotmail, outlook, gmail, etc." ) ;
			 	}
	 			new query[ 100 ];
				mysql_format( db, query, 256, "SELECT * FROM `users` WHERE `email` = '%s';", inputtext );
				mysql_query( db, query, true );
				if( cache_num_rows()) return SendClientMessage( playerid, 0xFF0000AA, "[ x ] Est� email j� est� cadastrado no nosso banco de dados.");
				// --
				PlayerTextDrawSetString( playerid, RegisterPlayer[ 2 ], inputtext );
				format( pInfo[ playerid ][ Email ], 129, "%s", inputtext );
				DigitouEmail[ playerid ] = true;
			}
			if( !response )
			{
				new Stringl[ 128 ];
				format( Stringl, sizeof Stringl, "{FFFFFF}Registrando seu email\n\tRegistre seu email para caso você esqueça sua {FF0000}'SENHA'{FFFFFF}\nVocê poderar recupera-la." ) ;
	 			ShowPlayerDialog( playerid, D_EMAIL , DIALOG_STYLE_INPUT , "{FFFFFF}Cadastro Email", Stringl, "Cadastrar", "Cancelar" ) ;
	 		}
			return 0x01;
		}
		case D_REGISTRO:
		{
			if( response )
			{
				format( pInfo[ playerid ][ pass ], 129, "%s", inputtext ) ;
				PasswordEncode( inputtext, strlen( inputtext ));
				// --
				PlayerTextDrawSetString( playerid, RegisterPlayer[ 0 ], inputtext );
		        DigitouSenha[ playerid ] = true, ConfSenha[ playerid ] = false;
			}
			else
			{
				Kick ( playerid  ) ;
			}
		}
	}
	return 0;
}
forward DadosRegistrados( playerid );
public DadosRegistrados( playerid )
{
	t_PainelOpen[ playerid ] = false;
	pInfo[ playerid ][ Codigo ] = cache_insert_id();
	Logado[playerid] = true;
	Registrado[playerid] = true;
	Novato[playerid] = true;
	
	new ano, mes, dia, hora, minuto, segundo;
	getdate( ano, mes, dia );
	gettime( hora, minuto, segundo );
	new date[ 84 ];
	format( date, 84, "%02d/%02d/%02d As %02d:%02d:%02d", dia, mes, ano, hora, minuto, segundo ) ;

    new query[ 200 ];
    mysql_format( db, query, sizeof query, "UPDATE `users` SET `UltimoLogin`='%s', `Register`='%s' WHERE `name` = '%e'", date, date, GetPlayerNameEx( playerid ));
    mysql_tquery( db, query, "", "" ) ;
    return 1;
}
