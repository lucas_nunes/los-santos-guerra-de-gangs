#include <YSI\y_hooks>
Autenticar_Register( playerid )
{
	for( new I; I < sizeof RegisterPlayer; I++) PlayerTextDrawShow( playerid, RegisterPlayer[ I ] );
    for( new I = 0; I < sizeof RegisterGlobal; I++) TextDrawShowForPlayer( playerid, RegisterGlobal[ I ] ) ;
	return 1;
}
hook OnGameModeInit( )
{
	RegisterGlobal[ 0 ] = TextDrawCreate(680.000000, -10.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 0 ], 255);
	TextDrawFont(RegisterGlobal[ 0 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 0 ], 0.500000, 55.000000);
	TextDrawColor(RegisterGlobal[ 0 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 0 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 0 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 0 ], 1);
	TextDrawUseBox(RegisterGlobal[ 0 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 0 ], 471604479);
	TextDrawTextSize(RegisterGlobal[ 0 ], -40.000000, 0.000000);
	TextDrawSetSelectable(RegisterGlobal[ 0 ], 0);

	RegisterGlobal[ 1 ] = TextDrawCreate(440.000000, 140.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 1 ], 255);
	TextDrawFont(RegisterGlobal[ 1 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 1 ], 0.000000, 22.000000);
	TextDrawColor(RegisterGlobal[ 1 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 1 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 1 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 1 ], 1);
	TextDrawUseBox(RegisterGlobal[ 1 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 1 ], 866844544);
	TextDrawTextSize(RegisterGlobal[ 1 ], 180.000000, 30.000000);
	TextDrawSetSelectable(RegisterGlobal[ 1 ], 0);

	RegisterGlobal[ 2 ] = TextDrawCreate(439.000000, 163.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 2 ], 255);
	TextDrawFont(RegisterGlobal[ 2 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 2 ], 0.560000, 19.399997);
	TextDrawColor(RegisterGlobal[ 2 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 2 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 2 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 2 ], 1);
	TextDrawUseBox(RegisterGlobal[ 2 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 2 ], 471604479);
	TextDrawTextSize(RegisterGlobal[ 2 ], 181.000000, -70.000000);
	TextDrawSetSelectable(RegisterGlobal[ 2 ], 0);

	RegisterGlobal[ 3 ] = TextDrawCreate(378.000000, 197.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 3 ], 255);
	TextDrawFont(RegisterGlobal[ 3 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 3 ], 0.560000, 1.399997);
	TextDrawColor(RegisterGlobal[ 3 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 3 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 3 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 3 ], 1);
	TextDrawUseBox(RegisterGlobal[ 3 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 3 ], -208);
	TextDrawTextSize(RegisterGlobal[ 3 ], 234.000000, -110.000000);
	TextDrawSetSelectable(RegisterGlobal[ 3 ], 0);

	RegisterGlobal[ 4 ] = TextDrawCreate(378.000000, 223.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 4 ], 255);
	TextDrawFont(RegisterGlobal[ 4 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 4 ], 0.560000, 1.399997);
	TextDrawColor(RegisterGlobal[ 4 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 4 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 4 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 4 ], 1);
	TextDrawUseBox(RegisterGlobal[ 4 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 4 ], -208);
	TextDrawTextSize(RegisterGlobal[ 4 ], 234.000000, -110.000000);
	TextDrawSetSelectable(RegisterGlobal[ 4 ], 0);

	RegisterGlobal[ 5 ] = TextDrawCreate(248.000000, 179.000000, "Cadastre-se para iniciar sua sessao:");
	TextDrawBackgroundColor(RegisterGlobal[ 5 ], 255);
	TextDrawFont(RegisterGlobal[ 5 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 5 ], 0.200000, 1.000000);
	TextDrawColor(RegisterGlobal[ 5 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 5 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 5 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 5 ], 1);
	TextDrawSetSelectable(RegisterGlobal[ 5 ], 0);

	RegisterGlobal[ 6 ] = TextDrawCreate(259.000000, 146.000000, "Los Santos Guerra de ~r~~h~GANGS");
	TextDrawBackgroundColor(RegisterGlobal[ 6 ], -251);
	TextDrawFont(RegisterGlobal[ 6 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 6 ], 0.200000, 1.000000);
	TextDrawColor(RegisterGlobal[ 6 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 6 ], 1);
	TextDrawSetProportional(RegisterGlobal[ 6 ], 1);
	TextDrawSetSelectable(RegisterGlobal[ 6 ], 0);

	RegisterGlobal[ 7 ] = TextDrawCreate(378.000000, 248.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 7 ], 255);
	TextDrawFont(RegisterGlobal[ 7 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 7 ], 0.560000, 1.399997);
	TextDrawColor(RegisterGlobal[ 7 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 7 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 7 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 7 ], 1);
	TextDrawUseBox(RegisterGlobal[ 7 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 7 ], -208);
	TextDrawTextSize(RegisterGlobal[ 7 ], 234.000000, -110.000000);
	TextDrawSetSelectable(RegisterGlobal[ 7 ], 0);

	RegisterGlobal[ 8 ] = TextDrawCreate(378.000000, 277.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 8 ], 255);
	TextDrawFont(RegisterGlobal[ 8 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 8 ], 0.790000, 2.799997);
	TextDrawColor(RegisterGlobal[ 8 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 8 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 8 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 8 ], 1);
	TextDrawUseBox(RegisterGlobal[ 8 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 8 ], 866844512);
	TextDrawTextSize(RegisterGlobal[ 8 ], 234.000000, -110.000000);
	TextDrawSetSelectable(RegisterGlobal[ 8 ], 0);

	RegisterGlobal[ 9 ] = TextDrawCreate(378.000000, 277.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 9 ], 255);
	TextDrawFont(RegisterGlobal[ 9 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 9 ], 0.790000, 1.199997);
	TextDrawColor(RegisterGlobal[ 9 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 9 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 9 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 9 ], 1);
	TextDrawUseBox(RegisterGlobal[ 9 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 9 ], 866844512);
	TextDrawTextSize(RegisterGlobal[ 9 ], 234.000000, -110.000000);
	TextDrawSetSelectable(RegisterGlobal[ 9 ], 0);

	RegisterGlobal[ 10 ] = TextDrawCreate(285.000000, 285.000000, "REGISTRAR");
	TextDrawBackgroundColor(RegisterGlobal[ 10 ], 255);
	TextDrawFont(RegisterGlobal[ 10 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 10 ], 0.200000, 1.000000);
	TextDrawColor(RegisterGlobal[ 10 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 10 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 10 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 10 ], 1);
	TextDrawTextSize( RegisterGlobal[ 10 ], 310.000000, 10.000000 ) ;
	TextDrawSetSelectable(RegisterGlobal[ 10 ], 1);

	RegisterGlobal[ 11 ] = TextDrawCreate(264.000000, 325.000000, "GTA BRASIL RPG 0.1 BETA");
	TextDrawBackgroundColor(RegisterGlobal[ 11 ], 255);
	TextDrawFont(RegisterGlobal[ 11 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 11 ], 0.200000, 1.000000);
	TextDrawColor(RegisterGlobal[ 11 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 11 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 11 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 11 ], 1);
	TextDrawSetSelectable(RegisterGlobal[ 11 ], 0);

	RegisterGlobal[ 12 ] = TextDrawCreate(440.000000, 154.000000, "_");
	TextDrawBackgroundColor(RegisterGlobal[ 12 ], 255);
	TextDrawFont(RegisterGlobal[ 12 ], 1);
	TextDrawLetterSize(RegisterGlobal[ 12 ], 0.790000, 0.699997);
	TextDrawColor(RegisterGlobal[ 12 ], -1);
	TextDrawSetOutline(RegisterGlobal[ 12 ], 0);
	TextDrawSetProportional(RegisterGlobal[ 12 ], 1);
	TextDrawSetShadow(RegisterGlobal[ 12 ], 1);
	TextDrawUseBox(RegisterGlobal[ 12 ], 1);
	TextDrawBoxColor(RegisterGlobal[ 12 ], 32);
	TextDrawTextSize(RegisterGlobal[ 12 ], 180.000000, -110.000000);
	TextDrawSetSelectable(RegisterGlobal[ 12 ], 0);
	return 1;
}
hook OnPlayerConnect( playerid )
{
	RegisterPlayer[ 0 ] = CreatePlayerTextDraw(playerid,243.000000, 198.000000, "Digite uma senha");
	PlayerTextDrawBackgroundColor(playerid,RegisterPlayer[ 0 ], 255);
	PlayerTextDrawFont(playerid,RegisterPlayer[ 0 ], 2);
	PlayerTextDrawLetterSize(playerid,RegisterPlayer[ 0 ], 0.200000, 1.000000);
	PlayerTextDrawColor(playerid,RegisterPlayer[ 0 ], -1);
	PlayerTextDrawSetOutline(playerid,RegisterPlayer[ 0 ], 0);
	PlayerTextDrawSetProportional(playerid,RegisterPlayer[ 0 ], 1);
	PlayerTextDrawSetShadow(playerid,RegisterPlayer[ 0 ], 1);
	PlayerTextDrawTextSize( playerid, RegisterPlayer[ 0 ], 385.000000, 10.000000 ) ;
	PlayerTextDrawSetSelectable(playerid,RegisterPlayer[ 0 ], 1);

	RegisterPlayer[ 1 ] = CreatePlayerTextDraw(playerid,243.000000, 224.000000, "Confirme sua senha");
	PlayerTextDrawBackgroundColor(playerid,RegisterPlayer[ 1 ], 255);
	PlayerTextDrawFont(playerid,RegisterPlayer[ 1 ], 2);
	PlayerTextDrawLetterSize(playerid,RegisterPlayer[ 1 ], 0.200000, 1.000000);
	PlayerTextDrawColor(playerid,RegisterPlayer[ 1 ], -1);
	PlayerTextDrawSetOutline(playerid,RegisterPlayer[ 1 ], 0);
	PlayerTextDrawSetProportional(playerid,RegisterPlayer[ 1 ], 1);
	PlayerTextDrawSetShadow(playerid,RegisterPlayer[ 1 ], 1);
	PlayerTextDrawTextSize( playerid, RegisterPlayer[ 1 ], 385.000000, 10.000000 ) ;
	PlayerTextDrawSetSelectable(playerid,RegisterPlayer[ 1 ], 1);

	RegisterPlayer[ 2 ] = CreatePlayerTextDraw(playerid,243.000000, 249.000000, "Digite um email valido");
	PlayerTextDrawBackgroundColor(playerid,RegisterPlayer[ 2 ], 255);
	PlayerTextDrawFont(playerid,RegisterPlayer[ 2 ], 1);
	PlayerTextDrawLetterSize(playerid,RegisterPlayer[ 2 ], 0.200000, 1.000000);
	PlayerTextDrawColor(playerid,RegisterPlayer[ 2 ], -1);
	PlayerTextDrawSetOutline(playerid,RegisterPlayer[ 2 ], 0);
	PlayerTextDrawSetProportional(playerid,RegisterPlayer[ 2 ], 1);
	PlayerTextDrawSetShadow(playerid,RegisterPlayer[ 2 ], 1);
	PlayerTextDrawTextSize( playerid, RegisterPlayer[ 2 ], 385.000000, 10.000000 ) ;
	PlayerTextDrawSetSelectable(playerid,RegisterPlayer[ 2 ], 1);
	return 1;
}



