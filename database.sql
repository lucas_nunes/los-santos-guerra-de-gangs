-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Jan 12, 2019 as 07:58 PM
-- Versão do Servidor: 5.5.10
-- Versão do PHP: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `mata`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `casas`
--

CREATE TABLE IF NOT EXISTS `casas` (
  `cID` int(11) NOT NULL AUTO_INCREMENT,
  `cProprietario` varchar(24) DEFAULT NULL,
  `cValor` int(15) DEFAULT NULL,
  `cInterior` int(5) DEFAULT NULL,
  `cVirtualWorld` int(5) DEFAULT NULL,
  `cComprado` tinyint(1) NOT NULL DEFAULT '0',
  `cLevel` int(5) NOT NULL DEFAULT '0',
  `cLevelMax` int(5) NOT NULL,
  `cPosX` float DEFAULT NULL,
  `cPosY` float DEFAULT NULL,
  `cPosZ` float DEFAULT NULL,
  `InteriorX` float DEFAULT NULL,
  `InteriorY` float DEFAULT NULL,
  `InteriorZ` float DEFAULT NULL,
  `InteriorA` float DEFAULT NULL,
  PRIMARY KEY (`cID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) NOT NULL,
  `email` varchar(64) NOT NULL,
  `pass` varchar(128) NOT NULL,
  `Grana` int(11) NOT NULL,
  `Admin` int(11) NOT NULL,
  `Score` int(11) NOT NULL,
  `Spree` int(11) NOT NULL,
  `gDominados` int(11) NOT NULL,
  `Procurado` int(11) NOT NULL,
  `SemLag` int(11) NOT NULL,
  `Mortes` int(11) NOT NULL,
  `VIP` int(11) NOT NULL,
  `VSegundos` int(11) NOT NULL,
  `VMinutos` int(11) NOT NULL,
  `VHoras` int(11) NOT NULL,
  `VDias` int(11) NOT NULL,
  `X1Wins` int(11) NOT NULL,
  `X1Loser` int(11) NOT NULL,
  `EventosWins` int(11) NOT NULL,
  `Visual` int(11) NOT NULL,
  `Preso` int(11) NOT NULL,
  `TempoPreso` int(11) NOT NULL,
  `HudColor` int(11) NOT NULL,
  `Register` varchar(24) NOT NULL,
  `UltimoLogin` varchar(24) NOT NULL,
  `FraseEntrada` varchar(128) NOT NULL DEFAULT 'N/A',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;